FROM node

RUN apt-get update \
           && apt-get install -y vim mlocate man-db \
           && npm install -g create-react-app