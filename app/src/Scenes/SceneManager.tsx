import { Scene } from 'phaser';

export default class SceneManager extends Scene {
    constructor(){
        super("SceneManager");
    }
    create(){
        /*
        Eventually, there will be a more sophisticated mechanism for choosing
        which scene to load first.
         */
        this.scene.start('UndergroundStage');
    }
    static getLevelString(level: integer, length: integer){
        return `${level}`.padStart(length, '0');
    }
}