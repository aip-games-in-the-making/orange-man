import {GameObjects, Physics, Types, Scene, Tilemaps} from "phaser";
import Spike from "../Sprites/Entities/NonLivingEntities/Spike";
import {Config} from "../Sprites/Entities/Entity";
import store from "../redux/store";
import {setLevel} from "../redux/level/slice";
import {PLAYER_INIT_HEALTH, PLAYER_MAX_HEALTH} from "../redux/player/slice";
import Enemy from "../Sprites/Entities/LivingEntities/Characters/Enemies/Enemy";
import Player from "../Sprites/Entities/LivingEntities/Characters/Player";
import Exit from "../Sprites/Entities/NonLivingEntities/Exit";
import {getTiledProperty} from "../Sprites/TiledSprite";

const HEART_WIDTH = 32;
const HEART_STEP = HEART_WIDTH + 5;

const HUD_PADDING = 30;

/**
 * Setup and organize data that is common to most if not all stages
 */
export default class Stage extends Scene{
    hud: GameObjects.Group | null = null;
    hazards: GameObjects.Group | null = null;
    /**
     * Initial level
     */
    level: integer = 1;
    stage: number = 1;
    player: Player | null = null;
    exit: Exit | null = null;
    cursors: Types.Input.Keyboard.CursorKeys | null = null;
    map: Tilemaps.Tilemap | null = null;
    enemies: GameObjects.Group | null = null;
    setLevel(level: integer){
        this.level = level;
        store.dispatch(setLevel(this.level));
    }
    setupCollisions(){
        // Entering an open door will send the player to the level assigned to the door
        if(this.exit && this.player){
            this.physics.add.overlap(this.exit, this.player, () => {
                if(this.cursors &&
                    this.cursors.up &&
                    this.cursors.up.isDown
                ){
                    // @ts-ignore
                    if(this.exit.canOpen() && this.exit.level){
                        // @ts-ignore
                        this.setLevel(this.exit.level);
                        this.scene.restart();
                    }

                }
            });
        }

        if(this.level === 1){
            this.hazards = this.add.group();
            // Get spawn points of spikes
            const spikeObjects: Types.Tilemaps.TiledObject[] | null = this.map && this.map.filterObjects(
                'Hazards',
                // @ts-ignore
                (obj: GameObjects.GameObject) => getTiledProperty('type', obj.properties) === 'spikes'
            );

            if(spikeObjects){
                spikeObjects.forEach((spike) => {
                    const {x, y, name, properties} = spike;

                    const type = getTiledProperty('type', properties)

                    const config: Config = {
                        scene: this,
                        x,
                        y,
                        name,
                        type,
                        frameType: type
                    };
                    const spikeSprite = new Spike(config);
                    this.hazards?.add(spikeSprite);
                });
            }

            if(this.hazards){
                //@ts-ignore
                this.hazards.getChildren().forEach((hazard: Physics.Arcade.Sprite) => {
                    hazard.anims.play('cycle', true);

                    // Determine if damage should be done from hazards
                    if(hazard instanceof Spike && this.player){
                        this.physics.add.overlap(hazard, this.player, () => {
                            if(!(hazard.canDamage && this.player?.canBeDamaged())){
                                return;
                            }
                            this.player?.doDamage(-1, hazard);
                        });
                    }
                });
            }
        }

        if(this.enemies){
            //@ts-ignore
            this.enemies.getChildren().forEach((enemy: Physics.Arcade.Sprite) => {
                // Determine if damage should be done from enemies
                if(enemy instanceof Enemy && this.player){
                    this.physics.add.overlap(enemy, this.player, () => {
                        if(!(enemy.canDamage && this.player?.canBeDamaged())){
                            return;
                        }
                        this.player?.doDamage(-1, enemy);
                    });
                }
            });
        }
    }
    preload(){
        this.setLevel(this.level);
        store.subscribe(() => {
            if(this.hud && this.hud.children){
                const {health} = store.getState().player;
                const activeHearts = this.hud?.getChildren().filter((heart: GameObjects.GameObject) => heart.active);
                if(activeHearts?.length !== health){
                    this.updateHud()
                }
            }
        })
       this.load.image('heart', 'assets/heart.png');
    }
    create(){

    }
    update(){
        this.enemies?.getChildren().forEach((enemy) => {
            const theEnemy = enemy as Enemy

            if(theEnemy.moving){
                theEnemy.move();
            } else {
                theEnemy.setVelocityX(0)
            }
        })
    }
    postCreate(){
        this.hud = this.add.group({
            key: 'heart',
            frameQuantity: PLAYER_INIT_HEALTH,
            maxSize: PLAYER_MAX_HEALTH,
            visible: false,
            active: false,
            setXY: {
                x: +this.game.config.width - PLAYER_INIT_HEALTH * HEART_STEP,
                y: HUD_PADDING,
                stepX: HEART_STEP
            },
            setScrollFactor: {
                x: 0,
                y: 0
            }
        } as Types.GameObjects.Group.GroupConfig);
        this.updateHud();
    }

    updateHud(){
        const {health} = store.getState().player;
        //@ts-ignore
        const hearts: Physics.Arcade.Sprite[] = this.hud?.getChildren();
        hearts.forEach((heart) => {
            this.hud?.killAndHide(heart);
        });

        //@ts-ignore
        const activeHearts: Physics.Arcade.Sprite[] = this.hud?.getChildren().slice(0, health);
        activeHearts.forEach((heart) => {
            heart.setActive(true);
            heart.setVisible(true);
        });
    }
}