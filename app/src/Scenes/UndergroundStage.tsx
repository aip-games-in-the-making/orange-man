import Phaser, {Animations, Tilemaps, Types, GameObjects} from 'phaser';
import Player from "../Sprites/Entities/LivingEntities/Characters/Player";
import Exit from "../Sprites/Entities/NonLivingEntities/Exit";
import Open from "../animations/Door/Open";
import Cycle from "../animations/Spike/Cycle";
import PlayerIdleLeft from "../animations/Player/Idle/Left";
import PlayerIdleRight from "../animations/Player/Idle/Right";
import PlayerWalkLeft from "../animations/Player/Walk/Left";
import PlayerWalkRight from "../animations/Player/Walk/Right";
import Wood from "../Sprites/Entities/NonLivingEntities/Cages/Wood";
import Metal from "../Sprites/Entities/NonLivingEntities/Cages/Metal";
import Bird from "../Sprites/Entities/LivingEntities/Critters/Bird";
import SceneManager from "./SceneManager";
import Bunny from "../Sprites/Entities/LivingEntities/Critters/Bunny";
import Critter, {Cage} from "../Sprites/Entities/LivingEntities/Critters/Critter";
import Lizard from "../Sprites/Entities/LivingEntities/Critters/Lizard";
import {Config} from "../Sprites/Entities/Entity";
import store from "../redux/store";
import {
    addDoors,
    clearDoors,
    openDoor,
    addCaptive,
    removeCaptive,
    clearCaptive
} from "../redux/level/slice";
import Heart from "../Sprites/Entities/NonLivingEntities/Pickups/Heart";
import Pickup from "../Sprites/Entities/NonLivingEntities/Pickups/Pickup";
import Flamo from "../Sprites/Entities/LivingEntities/Characters/Enemies/Flamo";
import Enemy from "../Sprites/Entities/LivingEntities/Characters/Enemies/Enemy";
import Stage from "./Stage";
import {getTiledProperty} from "../Sprites/TiledSprite";
import Blaze from "../Sprites/Entities/LivingEntities/Characters/Enemies/Blaze";

export interface TiledExit extends GameObjects.GameObject{
    x: integer;
    y: integer;
    name: 'Exit';
    type: 'Door';
    level: integer;
    properties: {value: any, name: string}[];
}

/**
 * Manage what will happen in underground stages
 */
export default class UndergroundStage extends Stage {
    groundLayer: Tilemaps.TilemapLayer | null = null;
    doors: GameObjects.Group | null = null;
    cages: GameObjects.Group | null = null;
    critters: GameObjects.Group | null = null;
    pickups: GameObjects.Group | null = null;
    exitAnimRunning: boolean = false;
    playerOverExit: boolean = false;
    constructor() {
        super("UndergroundStage");
    }
    preload(){
        super.preload()
        // tiles in spritesheet
        this.load.spritesheet('Solid Sets of Tiles', 'assets/Solid Sets of Tiles.png', {frameWidth: 32, frameHeight: 32});

        this.load.atlas('atlas', 'assets/atlas.png', 'assets/atlas.json');

        this.load.tilemapTiledJSON(`level01`, `assets/level01.json`);
        this.load.tilemapTiledJSON(`level02`, `assets/level02.json`);

        this.load.image('wood_cage', 'assets/wood_cage.png');
        this.load.image('metal_cage', 'assets/metal_cage.png');

        this.load.audio('heart', 'assets/heart.wav');
        this.load.audio('player_hurt', 'assets/player_hurt.wav');
        this.load.audio('open', 'assets/open.wav');
    }
    loadDoors(){
        // Create the doors sprites
        // Get spawn point of door
        const doorObjects: Types.Tilemaps.TiledObject[] = this.map!.filterObjects(
            'Doors',
            // @ts-ignore
            (obj) => getTiledProperty('type', obj.properties) === 'door'
        );

        this.doors = this.add.group();
        // Clear doors from Redux
        store.dispatch(clearDoors());

        // Find the level's exit
        const exitObject: Types.Tilemaps.TiledObject | undefined = doorObjects.find((door) => door.name === 'Exit');

        if(exitObject){
            const {x, y, properties, name} = exitObject;

            const type = getTiledProperty('type', properties);

            const config: Config = {
                scene: this,
                properties,
                name,
                x,
                y,
                type,
                frameType: type
            };
            this.exit = new Exit(config);
            if(this.doors){
                this.doors.add(this.exit);
                // Add doors to Redux
                const {name} = this.exit;
                store.dispatch(addDoors({name, open: false}));
            }
        }
    }
    loadCritters(){
        // Clear list of captives from the store
        store.dispatch(clearCaptive())
        this.critters = this.add.group();
        const crittersLayer = this.map?.getObjectLayer('Critters');
        if(crittersLayer){
            const crittersObjects: Types.Tilemaps.TiledObject[] = crittersLayer.objects;

            crittersObjects.forEach((critterObject) => {
                let sprite: Phaser.Physics.Arcade.Sprite | null = null;
                const {x, y, name, properties} = critterObject;

                const type = getTiledProperty('type', properties)

                const config: Config = {
                    scene: this,
                    x,
                    y,
                    name,
                    type,
                    frameType: type
                };
                switch(type){
                    case 'bird':
                        sprite = new Bird(config);
                        break;
                    case 'bunny':
                        sprite = new Bunny(config);
                        break;
                    case 'lizard':
                        sprite = new Lizard(config);
                        break;
                }
                if(sprite){
                    this.critters?.add(sprite);
                }
            });
        }
    }
    loadCages(){
        this.cages = this.add.group();
        const cagesLayer = this.map?.getObjectLayer('Cages');
        if(cagesLayer){
            let cageSprite: Phaser.Physics.Arcade.Sprite | null = null;
            cagesLayer.objects.forEach((cage: Types.Tilemaps.TiledObject) => {
                const {x, y, name, properties} = cage;

                const type = getTiledProperty('type', properties)

                const config: Config = {
                    scene: this,
                    x,
                    y,
                    name,
                    type,
                }
                switch(type){
                    case 'wood':
                        cageSprite = new Wood(config);
                        break;
                    case 'metal':
                        cageSprite = new Metal(config);
                        break;
                }
                if(cageSprite){
                    this.cages?.add(cageSprite as GameObjects.GameObject);
                }
            })
        }
    }
    loadPickups(){
        this.pickups = this.add.group();
        const pickupsLayer = this.map?.getObjectLayer('Pickups');
        if(pickupsLayer){
           let pickupSprite: Phaser.Physics.Arcade.Sprite | null = null;
            pickupsLayer.objects.forEach((pickup: Types.Tilemaps.TiledObject) => {
                const {x, y, name, properties} = pickup;

                const type = getTiledProperty('type', properties)

                const config: Config = {
                    scene: this,
                    x,
                    y,
                    name,
                    type,
                }
                switch (type) {
                    case 'heart':
                        pickupSprite = new Heart(config);
                        break;
                }
                if (pickupSprite) {
                    this.pickups?.add(pickupSprite as GameObjects.GameObject);
                }
            });
        }
    }
    loadEnemies(){
        this.enemies = this.add.group();
        const enemiesLayer = this.map?.getObjectLayer('Enemies');
        if(enemiesLayer){
            let enemySprite: Phaser.Physics.Arcade.Sprite | null = null;
            enemiesLayer.objects.forEach((enemy: Types.Tilemaps.TiledObject) => {
                const {x, y, name, properties} = enemy;

                const type = getTiledProperty('type', properties)

                const config: Config = {
                    scene: this,
                    x,
                    y,
                    name,
                    type,
                }
                switch (type) {
                    case 'flamo':
                        enemySprite = new Flamo(config);
                        break;
                    case 'blaze':
                        enemySprite = new Blaze(config);
                        break;
                }
                if (enemySprite) {
                    this.enemies?.add(enemySprite as GameObjects.GameObject);
                }
            });
        }
    }
    loadPlayer(){
        // Create the player sprite
        // Get spawn point
        const spawnPoint: Types.Tilemaps.TiledObject = this.map!.findObject(
            'Player',
            (obj: GameObjects.GameObject) => obj.name === 'Spawn Point'
        );
        const {x, y, properties} = spawnPoint;

        const type = getTiledProperty('type', properties)

        const config: Config = {
            scene: this,
            x,
            y,
            type
        }
        this.player = new Player(config);
        this.player.body.setSize(this.player.width, this.player.height - 30);
        this.player.setBounce(0.2); // our player will bounce
        this.player.setCollideWorldBounds(true); // don't go out of the map
        // @ts-ignore
        this.player.directionOfMovement = Phaser.NONE;
    }
    create(){
        super.create()
        // load the map
        const levelString = SceneManager.getLevelString(this.level, 2);
        this.map = this.make.tilemap({key: `level${levelString}`});
        // tiles for the ground layer

        const groundTiles = this.map.addTilesetImage('Solid Sets of Tiles');
        // create the ground layer
        this.groundLayer = this.map.createLayer('World', groundTiles, 0, 0);

        // the player will collide with this layer
        this.groundLayer.setCollisionByExclusion([-1]);

        // set the boundaries of our game world
        this.physics.world.bounds.width = this.groundLayer.width;
        this.physics.world.bounds.height = this.groundLayer.height;

        // set background color, so the sky is not black
        this.cameras.main.setBackgroundColor('#ccccff');

        // User Input
        this.cursors = this.input.keyboard.createCursorKeys();

        // Initialize all animations
        const playerAnimations = [
            new PlayerIdleLeft(this.anims),
            new PlayerIdleRight(this.anims),
            new PlayerWalkLeft(this.anims),
            new PlayerWalkRight(this.anims),
        ];

        const doorAnimations = [
            new Open(this.anims)
        ];

        const spikeAnimations = [
            new Cycle(this.anims)
        ];

        if(this.map) {
            this.loadPickups();
            this.loadDoors();
            this.loadCritters();
            this.loadCages();
            this.loadEnemies();
            this.loadPlayer();
        }

        // @ts-ignore
        const critterAnimations = this.critters?.getChildren().map((critter: Critter) => {
            critter.setCaptiveAnimations();
            return critter.getAnimations();
        }).flat();

        // @ts-ignore
        const enemyAnimations = this.enemies?.getChildren().map((enemy: Enemy) => {
            enemy.setAnimations();
            return enemy.getAnimations();
        }).flat();

        // Add all animations to scene
        const animations = [
            doorAnimations,
            spikeAnimations,
            playerAnimations,
            critterAnimations,
            enemyAnimations
        ];
        animations.forEach((animationGroup) => {
            // @ts-ignore
            animationGroup.forEach((animation: Animations.Animation) => {
                if(!this.anims.exists(animation.key)){
                    this.anims.add(animation.key, animation);
                }
            });
        });

        //this.anims.addMix(playerAnimations[1], playerAnimations[3], 5000)
        // Add all collisions to the scene
        this.setupSceneInteractions();

        this.postCreate()
    }
    addCageInteractions(cage: Cage){
        if(this.player){
            let cageY: number | null = null;
            // @ts-ignore
            this.physics.add.overlap(cage, this.player, () => {
                if(cageY){
                    return;
                }

                cageY = cage.y;
                this.tweens.add({
                    targets: cage,
                    y: cageY - 50,
                    duration: 2000,
                    ease: 'Elastic',
                    easeParams: [ 1.5, 0.5 ],
                    delay: 100,
                });

                // Release the critter upon opening the cage
                if(cage.critter){
                    cage.critter.playEscapeAnimation();
                    cage.sound.play();
                    // Remove the captivity from Redux
                    store.dispatch(removeCaptive({critter: cage.critter.name, cage: cage.name}));
                    // Open this scene's exit if possible
                    if(this.exit?.canOpen()){
                        store.dispatch(openDoor(this.exit.name));
                    }
                }
            })
        }
    }
    addCritterInteractions(critter: Critter){
        if(this.groundLayer){
            this.physics.add.collider(this.groundLayer, critter);
        }
    }

    addEnemyInteractions(enemy: Enemy){
        if(this.groundLayer){
            this.physics.add.collider(this.groundLayer, enemy);
        }
    }

    /**
     * Setup all collisions that will occur between entities
     */
    setupSceneInteractions(){
        if(this.player && this.groundLayer && this.map){
            // The player collides with the ground
            this.physics.add.collider(this.groundLayer, this.player);

            // set bounds so the camera won't go outside the game world
            this.cameras.main.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
            // make the camera follow the player
            this.cameras.main.startFollow(this.player);
        }

        // If overlap conditions are set earlier than here, they may not be applied for some reason

        // A player can open an exit door
        if(this.exit && this.player) {
            this.physics.add.overlap(this.exit, this.player, () => {
                if(!this.exit?.canOpen()) return;
                this.exit && this.exit.anims.play('open', true);
                this.playerOverExit = true;
            });
        }

        // Critters collide with the ground
        //@ts-ignore
        this.critters?.getChildren().forEach((critter: Critter) => {
            //@ts-ignore
            const cage: Cage = this.cages.getChildren().find((cage: Cage) => {
                return critter.name.split(' ')[1] === cage.name.split(' ')[1]
            });
            critter.cage = cage;
            cage.critter = critter;

            // Load cages and critters into Redux
            store.dispatch(addCaptive({
                cage: cage.name,
                critter: critter.name
            }));
            //@ts-ignore
            this.addCritterInteractions(critter);
            if(critter.type === 'bird'){
                critter.body.setSize(critter.width, critter.height - 10);
            }
            if(critter.type === 'lizard'){
                critter.body.setSize(critter.width, critter.height - 10);
            }
            critter.playCaptiveAnimation();
        })

        // The player can unlock cages
        if(this.cages?.children){
            //@ts-ignore
            this.cages.getChildren().forEach((cage: Cage) => {
                this.addCageInteractions(cage);
            })
        }

        // The player can pickup certain items
        if(this.pickups?.children){
            //@ts-ignore
            this.pickups.getChildren().forEach((pickup: Pickup) => {
                this.player?.enablePickup(pickup);
            })
        }

        if(this.enemies?.children){
            //@ts-ignore
            this.enemies?.getChildren().forEach((enemy: Enemy) => {
               this.addEnemyInteractions(enemy);
               enemy.playAnimations();
            });
        }

        // Finally, setup the main interactions defined in the parent class
        this.setupCollisions();
    }
}