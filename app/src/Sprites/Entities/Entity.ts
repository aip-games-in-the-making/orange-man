import {Scene} from "phaser";
import TiledSprite, {TiledProperty} from "../TiledSprite";

export interface Config {
    scene: Scene;
    x?: integer,
    y?: integer,
    name?: string;
    type?: string;
    frameType?: string;
    texture?: string;
    defaultFrame?: string;
    properties?: TiledProperty[];
}

export const DEFAULT_IDLE_KEY = 'idle';
/**
 * A TiledSprite that exists in the scene
 */
export default class Entity extends TiledSprite {
    // Original x-cord at which this Sprite spawned
    spawnX: number = 0
    constructor({
                    scene,
                    x = 0,
                    y = 0,
                    texture = 'atlas',
                    type = '',
                    frameType = '',
                    defaultFrame = DEFAULT_IDLE_KEY
    }: Config) {
        super(scene, x, y, texture);
        this.spawnX = x
        if (frameType && defaultFrame) {
            this.setFrame(`${frameType}/${defaultFrame}01.png`);
        }
        this.scene = scene;
        this.type = type;

        //add our entity to the scene
        this.scene.add.existing(this);
    }
}