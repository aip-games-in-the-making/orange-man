import LivingEntity from "../LivingEntity";
import {PlayerAnimationKey} from "./Player";
import {EnemyAnimationKey} from "./Enemies/Enemy";

export type CharacterAnimationKey = PlayerAnimationKey | EnemyAnimationKey;

export default class Character extends LivingEntity {
    public playAnimation(animation: CharacterAnimationKey){
        this.anims.play(animation, true);
    }
}