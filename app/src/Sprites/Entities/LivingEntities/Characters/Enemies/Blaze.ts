import Enemy from "./Enemy";
import {Config} from "../../../Entity";
import Walk from "../../../../../animations/Enemies/Blaze/Walk";
import {LEFT, RIGHT, Tweens} from "phaser";
import Idle from "../../../../../animations/Enemies/Blaze/Idle";

export default class Blaze extends Enemy{
    timeline: Tweens.Timeline | undefined;
    /**
     * The max distance from the spawn point that the enemy can move
     */
    movementRange: number = 50
    constructor(config: Config) {
        super({
            ...config,
            defaultFrame: `blaze/${Walk.key}`
        })
        this.setScale(1.2, 1.2);
    }

    move() {
        // Keep the sprite within the range of movement
        if(this.x >= this.spawnX + this.movementRange){
            this.directionOfMovement = LEFT
            this.x = this.spawnX + this.movementRange - 1
            this.flipX = true
            this.walkVelocity *= -1
        }

        if(this.x <= this.spawnX - this.movementRange){
            this.directionOfMovement = RIGHT
            this.x = this.spawnX - this.movementRange + 1
            this.flipX = false
            this.walkVelocity *= -1
        }

        // Make sure the correct direction is set
        if(this.walkVelocity <= 0){
            this.directionOfMovement = LEFT
        } else {
            this.directionOfMovement = RIGHT
        }

        this.setVelocityX(this.walkVelocity)
    }

    setAnimations() {
        this.animations = [
            new Walk(this.scene.anims),
            new Idle(this.scene.anims)
        ];
    }
    playAnimations(){
        this.playNeutralAnimation()
    }

    playNeutralAnimation(){
        const tweens = [
            // Idle
            {
                alpha: 1,
                onStart: () => {
                    this.moving = false
                    this.anims.play(`${this.type}_${Idle.key}`)
                },
                onComplete: () => {
                    this.moving = true
                    this.anims.play(`${this.type}_${Walk.key}`)
                },
                duration: 3000,
                repeat: 2
            },
            // Walk
            {
                alpha: 1,
                onStart: () => {
                    this.moving = true
                    this.anims.play(`${this.type}_${Walk.key}`)
                },
                onComplete: () => {
                    this.moving = false
                    this.anims.play(`${this.type}_${Idle.key}`)
                },
                duration: 6000
            }
        ]

        this.timeline = this.scene.tweens.timeline({
            targets: this,
            loop: -1,
            tweens,
        });
    }

}