import Character from "../Character";
import {Config, DEFAULT_IDLE_KEY} from "../../../Entity";
import {Animations} from "phaser";

export type EnemyAnimationKey = EnemyWalkAnimationKey;
export type EnemyWalkAnimationKey = 'walk';
export type EnemyIdleAnimationKey = typeof DEFAULT_IDLE_KEY;

export default class Enemy extends Character{
    animations: Animations.Animation[] = [];
    canDamage: boolean = true;
    /**
     * Whether the enemy is currently moving
     */
    moving = false;
    /**
     * How fast the enemy walks
     */
    walkVelocity = 20
    damageType: string = 'pushback';
    constructor(config: Config) {
        super({
            ...config,
            frameType: 'enemies'
        })
    }
    // Physically move the sprite
    move() {

    }
    /**
     * Populate the animations array
     */
    setAnimations(){

    }

    getAnimations(){
        return this.animations;
    }

    playAnimations(){

    }
}