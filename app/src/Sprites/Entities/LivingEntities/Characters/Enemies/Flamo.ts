import Enemy, {EnemyWalkAnimationKey} from "./Enemy";
import {Config} from "../../../Entity";
import Walk from "../../../../../animations/Enemies/Flamo/Walk";
import {LEFT, RIGHT} from "phaser";

export default class Flamo extends Enemy{
    moving = true
    constructor(config: Config) {
        super({
            ...config,
            defaultFrame: `flamo/${Walk.key}`
        })
        this.setScale(1.2, 1.2);
    }
    setAnimations() {
        this.animations = [
            new Walk(this.scene.anims)
        ];
    }
    playAnimations(){
        this.walk();
    }
    playWalkAnimation(animation: EnemyWalkAnimationKey){
        this.playAnimation(`${this.type}_${animation}`);
    }
    // Start walkin'
    walk(){
        this.playWalkAnimation(Walk.key);
        this.setVelocityX(-this.walkVelocity);
        this.directionOfMovement = LEFT;
        this.scene.time.addEvent({
            delay: 3000,
            callback: () => {
                // Change the direction of movement
                if(this.body.velocity.x < 0){
                    this.directionOfMovement = RIGHT;
                } else {
                    this.directionOfMovement = LEFT;
                }
                this.setVelocityX(-this.body.velocity.x);
            },
            repeat: -1
        });
    }
}