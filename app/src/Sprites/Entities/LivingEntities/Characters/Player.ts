import Entity, {Config} from "../../Entity";
import {createSelector} from "@reduxjs/toolkit";
import store, {RootState} from "../../../../redux/store";
import {toggleDamage, adjustHealth, setHealth, PLAYER_INIT_HEALTH} from "../../../../redux/player/slice";
import Pickup from "../../NonLivingEntities/Pickups/Pickup";
import Character from "./Character";
import WalkLeft from "../../../../animations/Player/Walk/Left";
import WalkRight from "../../../../animations/Player/Walk/Right";
import IdleLeft from "../../../../animations/Player/Idle/Left";
import IdleRight from "../../../../animations/Player/Idle/Right";
import {Sound, Scenes} from "phaser";
import UndergroundStage from "../../../../Scenes/UndergroundStage";

export type PlayerWalkAnimationKey = typeof WalkLeft.key | typeof WalkRight.key;
export type PlayerIdleAnimationKey = typeof IdleLeft.key | typeof IdleRight.key;
export type PlayerAnimationKey = PlayerWalkAnimationKey | PlayerIdleAnimationKey;

export interface Sounds{
    [key: string]: Sound.BaseSound;
}

export default class Player extends Character {
    constructor(config: Config) {
        super({
            ...config,
            defaultFrame: 'mp_idle_right_'
        })
        this.setListeners();
    }
    idleDelay = 3000;
    setListeners(){
        this.scene.events.on(Scenes.Events.UPDATE, this.inputListener, this)
    }
    inputListener(){
        const scene = this.scene ? this.scene as UndergroundStage : null;

        if(scene && scene.cursors && scene.player && !(scene.player.body instanceof Phaser.Physics.Arcade.StaticBody)){
            if (scene.cursors.left && scene.cursors.left.isDown)
            {
                // move left
                scene.player.body.setVelocityX(-200);
                (!this.anims.isPlaying || this.anims.currentAnim.key !== 'walk_left') &&  scene.player.playWalkAnimation('walk_left'); // play walk animation

                scene.player.directionOfMovement = Phaser.LEFT;
            }
            else if (scene.cursors.right && scene.cursors.right.isDown)
            {
                // move right
                scene.player.body.setVelocityX(200);
                (!this.anims.isPlaying || this.anims.currentAnim.key !== 'walk_right') && scene.player.playWalkAnimation('walk_right'); // play walk animation

                scene.player.directionOfMovement = Phaser.RIGHT;
            } else {
                // stay still
                scene.player.body.setVelocityX(0);

                if(scene.player.directionOfMovement === Phaser.LEFT){
                    (!this.anims.isPlaying || this.anims.currentAnim.key !== 'idle_left') && scene.player.playIdleAnimation('idle_left');
                } else {
                    (!this.anims.isPlaying || this.anims.currentAnim.key !== 'idle_right') && scene.player.playIdleAnimation('idle_right');
                }
            }

            if (scene.cursors.space &&
                scene.cursors.up &&
                (scene.cursors.space.isDown || scene.cursors.up.isDown) &&
                scene.player.body.onFloor()
            )
            {
                // Kris Kross will make you...
                scene.player.body.setVelocityY(-500);
            }
        }

        if(scene && !scene.playerOverExit){
            if(scene.exit){
                scene.exit.anims.stop();
                if(scene.exit.anims.currentAnim){
                    scene.exit.anims.restart();
                }
            }
        }
        if(scene){
            scene.exitAnimRunning = false;
            scene.playerOverExit = false;
        }

    }
    sounds: Sounds = {
        playerHurt: this.scene.sound.add('player_hurt')
    };
    playWalkAnimation(animation: PlayerWalkAnimationKey){
        this.playAnimation(animation);
    }
    playIdleAnimation(animation: PlayerIdleAnimationKey){
        this.playAnimation(animation);
        this.anims.nextTick += IdleLeft.idleDelay;
    }
    playDamageAnimation(hazard: Entity){
        //@ts-ignore
        if(hazard.damageType === 'pushback'){
            //@ts-ignore
            const xOrient: integer = this.body.center.x - hazard.body.center.x;
            this.setVelocity(0,0);
            this.setAlpha(0);

            this.scene.tweens.add({
                targets: this,
                alpha: 1,
                duration: 100,
                ease: 'Linear',
                repeat: 5,
            });

            this.scene.tweens.add({
                targets: this,
                x: this.x + (5 * (xOrient < 1 ? -1 : 1)),
                y: this.y - 5,
                duration: 100,
                ease: 'Linear',
            });
        }
    }

    /**
     * Enable the ability to pickup the given item
     */
    enablePickup(pickup: Pickup){
        this.scene.physics.add.overlap(this, pickup, () => {
            pickup.doEffect(this);
            pickup.destroy();
        });
    }

    /**
     * Set whether or not this player can be damaged
     *
     * @param toggle
     */
    toggleDamage(toggle: boolean){
        if(this.canBeDamaged() !== toggle){
            store.dispatch(toggleDamage(toggle));
        }
    }
    /** Return whether or not this player can be damaged **/
    canBeDamaged(): boolean{
        const playerCanBeDamagedSelector = createSelector(
            (state: RootState) => state.player.canBeDamaged,
            (canBeDamaged: boolean): boolean => canBeDamaged
        );
        return playerCanBeDamagedSelector(store.getState())
    }

    /**
     * Heal the player
     */
    heal(healAmount: integer){
        const amount = Math.abs(healAmount);
        store.dispatch(adjustHealth(amount));
    }

    /**
     * Do damage to the player
     * @param damageAmount
     * @param hazard
     */
    doDamage(damageAmount: integer, hazard: Entity){
        if(!this.canBeDamaged()){
            return;
        }
        // Make sure damageAmount is always negative
        if(damageAmount > 0){
            damageAmount *= -1;
        }
        store.dispatch(adjustHealth(damageAmount));
        if(store.getState().player.health < 0){
            this.endScene();
            return;
        }

        this.playDamageAnimation(hazard);
        this.sounds.playerHurt.play();

        this.toggleDamage(false);
        this.scene.time.addEvent({
            delay: 3000, // Amount of milliseconds before player can take damage again
            repeat: 0,
            callback: () => {
                this.toggleDamage(true);
            }
        });
    }

    /**
     * The player can no longer be in the scene, e.g. due to death. The player and scene must be reset.
     */
    endScene(){
        store.dispatch(setHealth(PLAYER_INIT_HEALTH));
        // The player can take damage again when the scene restarts
        this.toggleDamage(true);
        this.scene.scene.restart();
    }
}