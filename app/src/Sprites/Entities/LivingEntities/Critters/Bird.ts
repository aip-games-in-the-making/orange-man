import Critter, {CritterAnimationKey} from './Critter';
import Fly from "../../../../animations/Bird/Fly";
import Idle from "../../../../animations/Bird/Idle";
import Walk from "../../../../animations/Bird/Walk";

// The animations this sprite supports
export type BirdAnimationKey = 'walk' | 'idle' | 'fly';

export default class Bird extends Critter {
    escapeAnimationKey: CritterAnimationKey = Fly.key;
    idleDelay = Idle.idleDelay;
    setCaptiveAnimations(){
        this.animations = [
            new Idle(this.scene.anims),
            new Walk(this.scene.anims),
            new Fly(this.scene.anims)
        ];
    }
    playEscapeAnimation(){
        super.playEscapeAnimation({movementTween: {y: this.y - 150}});
    }
}