import Critter, {CritterAnimationKey, CritterIdleAnimationKey} from "./Critter";
import Idle from "../../../../animations/Bunny/Idle";
import Walk from "../../../../animations/Bunny/Walk";
import Escape from "../../../../animations/Bunny/Escape";
import {Config} from "../../Entity";

// The animations this sprite supports
export type BunnyAnimationKey = 'walk' | 'idle';

export default class Bunny extends Critter{
    idleAnimationKey: CritterIdleAnimationKey = Idle.key;
    escapeAnimationKey: CritterAnimationKey = Escape.key;
    idleAnimationData = {duration: 2000}
    idleDelay = Idle.idleDelay;
    captiveAnimationData = {duration: 3000}
    constructor(config: Config) {
        super({
            ...config,
            defaultFrame: Walk.key
        });
    }
    setCaptiveAnimations(){
        this.animations = [
            new Idle(this.scene.anims),
            new Walk(this.scene.anims),
            new Escape(this.scene.anims)
        ]
    }
}