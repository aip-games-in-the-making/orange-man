import {Animations, LEFT, NONE, RIGHT, Tweens, Types} from "phaser";
import Metal from "../../NonLivingEntities/Cages/Metal";
import Wood from "../../NonLivingEntities/Cages/Wood";
import {Config, DEFAULT_IDLE_KEY} from "../../Entity";
import LivingEntity from "../LivingEntity";

export type CritterIdleAnimationKey = typeof DEFAULT_IDLE_KEY;
export type CritterAnimationKey = 'walk' | CritterIdleAnimationKey | 'fly' | 'escape';
export type Cage = Metal | Wood

export default class Critter extends LivingEntity {
    timeline: Tweens.Timeline | undefined;
    cage: Cage | undefined;
    walkAnimationKey: CritterAnimationKey = 'walk';
    idleAnimationKey: CritterIdleAnimationKey = DEFAULT_IDLE_KEY;
    idleAnimationData: {} = {duration: 7000};
    captiveAnimationData: Types.Tweens.TimelineBuilderConfig = {};
    escapeAnimationKey: CritterAnimationKey = 'escape';
    animations: Animations.Animation[] = [];
    name: string;
    constructor(config: Config) {
        super(config);
        const {name = ''} = config;
        this.name = name;
    }

    /**
     * Populate the animations array
     */
    setCaptiveAnimations(){

    }
    getAnimations(){
        return this.animations;
    }

    playAnimation(animation: CritterAnimationKey, direction: integer) {
        this.directionOfMovement = direction;
        this.anims.play(`${this.type}_${animation}`);
    }

    playIdleAnimation(animation: CritterIdleAnimationKey, direction: integer){
        this.playAnimation(animation, direction);
        this.anims.nextTick += this.idleDelay;
    }

    playCaptiveAnimation(tweens: object[] = []) {
        const {walkAnimationKey, idleAnimationKey, x} = this;
        const xLeft = x - 20;
        const xRight = x + 20;

        // If no tweens given, then create the default
        if (tweens.length === 0) {
            tweens = [
                // Idle
                {
                    x,
                    onStart: () => {
                        this.playIdleAnimation(idleAnimationKey, NONE)
                    },
                    onComplete: () => {
                        this.playAnimation(walkAnimationKey, LEFT)
                    },
                    ...this.idleAnimationData
                },
                // Move Left
                {
                    yoyo: true,
                    flipX: true,
                    x: xLeft,
                    onYoyo: () => {
                        this.directionOfMovement = RIGHT;
                    },
                    onComplete: () => {
                        this.playAnimation(walkAnimationKey, RIGHT)
                    }
                },
                // Move Right
                {
                    yoyo: true,
                    flipX: true,
                    x: xRight,
                    onYoyo: () => {
                        this.directionOfMovement = LEFT;
                    },
                    onComplete: () => {
                        this.playIdleAnimation(idleAnimationKey, NONE)
                    }
                }
            ];
        }

        this.timeline = this.scene.tweens.timeline({
            targets: this,
            ease: 'Linear',
            loop: -1,
            tweens,
            ...this.captiveAnimationData
        });
    }

    playEscapeAnimation(escapeTweenData: {movementTween?: {}, appearanceTween?: {}} = {}) {
        const {movementTween, appearanceTween} = escapeTweenData;
        const {escapeAnimationKey, directionOfMovement, timeline, scene, x} = this;
        if (timeline) {
            timeline.stop();
        }
        scene.time.addEvent({
            delay: 500,
            callback: () => {
                // The critter escapes in whatever direction they were last moving
                this.playAnimation(escapeAnimationKey, directionOfMovement);
                scene.tweens.add({
                    targets: this,
                    alpha: 0,
                    duration: 200,
                    repeat: 6,
                    ...appearanceTween
                });
                scene.tweens.add({
                    targets: this,
                    x: x + ((directionOfMovement === LEFT || directionOfMovement === NONE ) ? -200 : 200),
                    duration: 1000,
                    ...movementTween,
                    onComplete: () => {
                        this.destroy();
                    }
                })
            }
        })
    }
}