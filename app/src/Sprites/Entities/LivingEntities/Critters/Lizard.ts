import Critter, {CritterAnimationKey, CritterIdleAnimationKey} from "./Critter";
import Idle from "../../../../animations/Lizard/Idle";
import Walk from "../../../../animations/Lizard/Walk";
import Escape from "../../../../animations/Lizard/Escape";
import {Config} from "../../Entity";

// The animations this sprite supports
export type LizardAnimationKey = 'walk' | CritterIdleAnimationKey;

export default class Lizard extends Critter{
    idleDelay = Idle.idleDelay;
    idleAnimationKey: CritterIdleAnimationKey = Idle.key;
    idleAnimationData: {} = {duration: 5000}
    escapeAnimationKey: CritterAnimationKey = Escape.key;
    constructor(config: Config) {
        super({
            ...config,
            defaultFrame: Walk.key
        });
    }
    setCaptiveAnimations(){
        this.animations = [
            new Idle(this.scene.anims),
            new Walk(this.scene.anims),
            new Escape(this.scene.anims)
        ]
    }
}