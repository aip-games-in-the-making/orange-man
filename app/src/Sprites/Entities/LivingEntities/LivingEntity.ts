import Entity, {Config} from "../Entity";
import {NONE, LEFT, RIGHT, UP, DOWN} from "phaser";

export type DirectionOfMovement = typeof NONE | typeof LEFT | typeof RIGHT | typeof UP | typeof DOWN;

/**
 * An Entity that moves and is effected by physics
 */
export default class LivingEntity extends Entity {
    directionOfMovement: DirectionOfMovement = NONE;
    prevDirectionOfMovement: DirectionOfMovement = NONE;
    // How long to wait before playing the entity's idle animation
    idleDelay: number = 0;
    constructor(config: Config) {
        super(config);

        //enable physics
        this.scene.physics.world.enable(this);
    }
}