import Critter from "../../LivingEntities/Critters/Critter";
import {Config} from "../../Entity";
import NonLivingEntity from "../NonLivingEntity";
import {Sound} from "phaser";

/**
 * A cage holding something like a critter captive
 */
export default class Cage extends NonLivingEntity {
    critter: Critter | undefined;
    sound: Sound.BaseSound = this.scene.sound.add('open');
    constructor(config: Config) {
        super(config);
        const {name = ''} = config;
        this.name = name;
    }
}