import Cage from "./Cage";
import {Config} from "../../Entity";

/**
 * A metal cage holding a critter captive
 */
export default class Metal extends Cage {
    constructor(config: Config) {
        super({
            ...config,
            texture: 'metal_cage'
        });
    }
}