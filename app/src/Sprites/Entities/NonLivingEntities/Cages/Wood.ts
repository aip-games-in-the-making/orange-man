import {Config} from "../../Entity";
import Cage from "./Cage";

/**
 * A wooden cage holding a critter captive
 */
export default class Wood extends Cage {
    constructor(config: Config) {
        super({
            ...config,
            texture: 'wood_cage'
        });
    }
}