import {Config} from "../Entity";
import {createSelector} from "@reduxjs/toolkit";
import store, {RootState} from "../../../redux/store";
import NonLivingEntity from "./NonLivingEntity";

/**
 * A door that lets you leave the level
 */
export default class Exit extends NonLivingEntity {
    /**
     * The level that this exit leads to
     */
    level: integer | undefined;
    constructor(config: Config) {
        super({
            ...config,
            type: 'door',
            defaultFrame: 'door'
        })
        const {name = '', properties} = config;
        this.name = name;
        this.properties = properties;

        this.level = this.getProperty('level');
    }
    canOpen(): boolean{
        const captiveSelector = createSelector(
            (state: RootState) => state.level.captive,
            captive => captive
        );
        return captiveSelector(store.getState()).length === 0;
    }
}