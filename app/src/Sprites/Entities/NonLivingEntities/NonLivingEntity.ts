import Entity, {Config} from "../Entity";
import {Physics} from "phaser";

/**
 * An Entity that moves and is effected by physics
 */
export default class NonLivingEntity extends Entity {
    constructor(config: Config) {
        super(config);

        //enable physics
        this.scene.physics.world.enable(this, Physics.Arcade.STATIC_BODY)
    }
}