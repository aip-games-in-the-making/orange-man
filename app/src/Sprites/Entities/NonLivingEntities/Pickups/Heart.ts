import {Sound} from "phaser";
import {Config} from "../../Entity";
import Pickup, {Effect} from "./Pickup";

/**
 * A heart to refill health
 */
export default class Heart extends Pickup {
    effectAmount: number = 1;
    effectType: Effect = 'heal';
    sound: Sound.BaseSound = this.scene.sound.add('heart');
    constructor(config: Config) {
        super({
            ...config,
            texture: 'heart'
        });
    }
}