import {Config} from "../../Entity";
import NonLivingEntity from "../NonLivingEntity";
import Player from "../../LivingEntities/Characters/Player";
import {Sound} from "phaser";

export type Effect = 'heal' | 'damage' | '';

/**
 * An item the player can pickup
 */
export default class Pickup extends NonLivingEntity {
    effectType: Effect = '';
    effectAmount: number = 0;
    sound: Sound.BaseSound | null = null;
    constructor(config: Config) {
        super(config);
        const {name = ''} = config;
        this.name = name;
    }
    /**
     * Do the thing that the pickup is meant to do to the player
     */
    doEffect(player: Player){
        if(this.sound){
            this.sound.play();
        }
        switch(this.effectType){
            case 'heal':
                player.heal(this.effectAmount);
            break;
        }
    }
}