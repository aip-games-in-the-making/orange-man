import { Animations } from 'phaser';
import {Config} from "../Entity";
import NonLivingEntity from "./NonLivingEntity";

/**
 * A set of spikes that will harm the player
 */
export default class Spike extends NonLivingEntity {
    canDamage: boolean = false;
    damageType: string = 'pushback'; // Pushes the player up and back a little
    constructor(config: Config) {
        super({
            ...config,
            type: 'spikes',
            defaultFrame: 'spikes'
        })
        const {name = ''} = config;
        this.name = name;

        this.body.setSize(this.width, 10);
        this.body.setOffset(0, 20);

        // The hazard can damage only when it appears
        this.on('animationstart', (animation: Animations.Animation) => {
            if(animation.key === 'cycle'){
                this.canDamage = true;
            }

        });
        this.on('animationrepeat', (animation: Animations.Animation) => {
            if(animation.key === 'cycle'){
                this.canDamage = true;
            }
        });
        this.on('animationupdate', (animation: Animations.Animation, frame: Animations.AnimationFrame) => {
            // The animation pauses at the second-to-last frame
            if(animation.key === 'cycle') {
                if (this.canDamage && (frame.index === this.anims.getTotalFrames() - 2)) {
                    this.canDamage = false;
                }
            }
        });
    }

}