import { Physics } from 'phaser';

export interface TiledProperty {
    name: string;
    type: string;
    value: any;
}

export default class TiledSprite extends Physics.Arcade.Sprite {
    properties: TiledProperty[] | undefined;
    getProperty(propName: string): any {
        return getTiledProperty(propName, this.properties!)
    }
}

export const getTiledProperty = (propName: string, properties: TiledProperty[]): any => {
    let value = null;
    if(properties.length > 0){
        value = properties.find((prop ) => prop.name === propName)?.value;
    }
    return value;
}