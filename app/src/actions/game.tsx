import {Game} from 'phaser';

export const GET_GAME = 'GET_GAME';
export const SET_GAME = 'SET_GAME';

export const getGame = (game: Game) => ({
    type: GET_GAME,
    game
});

export const setGame = (game: Game) => ({
    type: SET_GAME,
    game
});