import {Animations, Types} from 'phaser';
import {BirdAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Bird";

export default class Fly extends Animations.Animation {
    static key: BirdAnimationKey = 'fly'
    constructor(manager: Animations.AnimationManager) {
        const { key } = Fly;
        const animationKey = `bird_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `bird/${key}`,
                suffix: '.png',
                start: 1,
                end: 2,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            yoyo: true
        };
        super(manager, animationKey, config);
    }
}