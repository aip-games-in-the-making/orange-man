import {Animations, Types} from 'phaser';
import {BirdAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Bird";

export default class Idle extends Animations.Animation {
    static key: BirdAnimationKey = 'idle';
    static idleDelay: number = 2000;
    constructor(manager: Animations.AnimationManager) {
        const { key } = Idle;
        const animationKey = `bird_${key}`;
        const config: Types.Animations.Animation = {
            key: animationKey,
            frames: manager.generateFrameNames('atlas', {
                prefix: `bird/${key}`,
                suffix: '.png',
                start: 1,
                end: 4,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: 2,
            repeatDelay: 3000,
            yoyo: true,
        };
        super(manager, animationKey, config);
    }
}