import {Animations, Types} from 'phaser';
import {BirdAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Bird";

export default class Walk extends Animations.Animation {
    static key: BirdAnimationKey = 'walk';
    constructor(manager: Animations.AnimationManager) {
        const { key } = Walk;
        const animationKey = `bird_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `bird/${key}`,
                suffix: '.png',
                start: 1,
                end: 3,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            yoyo: true
        };
        super(manager, animationKey, config);
    }
}