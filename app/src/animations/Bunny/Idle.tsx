import {Animations, Types} from 'phaser';
import {CritterIdleAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Critter";

export default class Idle extends Animations.Animation {
    static key: CritterIdleAnimationKey = 'idle';
    static idleDelay: number = 2000;
    constructor(manager: Animations.AnimationManager) {
        const { key } = Idle;
        const animationKey = `bunny_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `bunny/walk`,
                suffix: '.png',
                start: 1,
                end: 1,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: 2,
            repeatDelay: 3000,
            yoyo: true
        };
        super(manager, animationKey, config);
    }
}