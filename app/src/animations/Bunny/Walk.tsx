import {Animations, Types} from 'phaser';
import {BunnyAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Bunny";

export default class Walk extends Animations.Animation {
    static key: BunnyAnimationKey = 'walk';
    constructor(manager: Animations.AnimationManager) {
        const { key } = Walk;
        const animationKey = `bunny_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `bunny/${key}`,
                suffix: '.png',
                start: 1,
                end: 6,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
        };
        super(manager, animationKey, config);
    }
}