import {Animations, Types} from 'phaser';

export default class Open extends Animations.Animation {
    constructor(manager: Animations.AnimationManager) {
        const config: Types.Animations.Animation = {
            key: 'open',
            frames: manager.generateFrameNames('atlas',
                {
                    prefix: 'door/door',
                    suffix: '.png',
                    start: 1,
                    end: 10,
                    zeroPad: 2
                }),
            frameRate: 10,
            repeat: -1,
        };
        const key: string = 'open';
        super(manager, key, config);
    }
}