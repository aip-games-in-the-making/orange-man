import {Animations, Types} from 'phaser';
import {EnemyIdleAnimationKey} from "../../../Sprites/Entities/LivingEntities/Characters/Enemies/Enemy";

export default class Idle extends Animations.Animation {
    static key: EnemyIdleAnimationKey = 'idle';
    constructor(manager: Animations.AnimationManager) {
        const key = `blaze_${Idle.key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `enemies/blaze/${Idle.key}`,
                suffix: '.png',
                start: 1,
                end: 15,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            yoyo: true
        };
        super(manager, key, config);
    }
}