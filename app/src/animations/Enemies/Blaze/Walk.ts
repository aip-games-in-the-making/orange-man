import {Animations, Types} from 'phaser';
import {EnemyWalkAnimationKey} from "../../../Sprites/Entities/LivingEntities/Characters/Enemies/Enemy";

export default class Walk extends Animations.Animation {
    static key: EnemyWalkAnimationKey = 'walk';
    constructor(manager: Animations.AnimationManager) {
        const key = `blaze_${Walk.key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `enemies/blaze/${Walk.key}`,
                suffix: '.png',
                start: 1,
                end: 3,
                zeroPad: 2
            }),
            frameRate: 7,
            repeat: -1
        };
        super(manager, key, config);
    }
}