import {Animations, Types} from 'phaser';
import {EnemyWalkAnimationKey} from "../../../Sprites/Entities/LivingEntities/Characters/Enemies/Enemy";

export default class Walk extends Animations.Animation {
    static key: EnemyWalkAnimationKey = 'walk';
    constructor(manager: Animations.AnimationManager) {
        const key = `flamo_${Walk.key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `enemies/flamo/${Walk.key}`,
                suffix: '.png',
                start: 1,
                end: 4,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1
        };
        super(manager, key, config);
    }
}