import {Animations, Types} from 'phaser';
import {LizardAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Lizard";

export default class Escape extends Animations.Animation {
    static key: LizardAnimationKey = 'walk'
    constructor(manager: Animations.AnimationManager) {
        const { key } = Escape;
        const animationKey = `lizard_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `lizard/${key}`,
                suffix: '.png',
                start: 1,
                end: 5,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            yoyo: true
        };
        super(manager, animationKey, config);
    }
}