import {Animations, Types} from 'phaser';
import {CritterIdleAnimationKey} from "../../Sprites/Entities/LivingEntities/Critters/Critter";

export default class Idle extends Animations.Animation {
    static key: CritterIdleAnimationKey = 'idle';
    static idleDelay: number = 1000;
    constructor(manager: Animations.AnimationManager) {
        const { key } = Idle;
        const animationKey = `lizard_${key}`;
        const config: Types.Animations.Animation = {
            frames: manager.generateFrameNames('atlas', {
                prefix: `lizard/${key}`,
                suffix: '.png',
                start: 1,
                end: 4,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: 2,
            repeatDelay: 1000,
            yoyo: true
        };
        super(manager, animationKey, config);
    }
}