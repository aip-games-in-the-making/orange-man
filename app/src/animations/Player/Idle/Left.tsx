import {Animations, Types} from 'phaser';

export default class Left extends Animations.Animation {
    static key = 'idle_left';
    static idleDelay: number = 3000;
    constructor(manager: Animations.AnimationManager) {
        const { key } = Left;
        const config: Types.Animations.Animation = {
            key,
            frames: manager.generateFrameNames('atlas', {
                prefix: `player/mp_${key}_`,
                suffix: '.png',
                start: 1,
                end: 2,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            repeatDelay: 5000,
            yoyo: true
        };
        super(manager, key, config);
    }
}