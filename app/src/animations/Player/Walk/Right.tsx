import {Animations, Types} from 'phaser';

export default class Right extends Animations.Animation {
    static key = 'walk_right';
    constructor(manager: Animations.AnimationManager) {
        const { key } = Right;
        const config: Types.Animations.Animation = {
            key,
            frames: manager.generateFrameNames('atlas', {
                prefix: `player/mp_${key}_`,
                suffix: '.png',
                start: 1,
                end: 4,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1
        };
        super(manager, key, config);
    }
}