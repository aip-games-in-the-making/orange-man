import {Animations, Types} from 'phaser';

export default class Cycle extends Animations.Animation {
    constructor(manager: Animations.AnimationManager) {
        const config: Types.Animations.Animation = {
            key: 'cycle',
            frames: manager.generateFrameNames('atlas', {
                prefix: 'spikes/spikes',
                suffix: '.png',
                start: 1,
                end: 9,
                zeroPad: 2
            }),
            frameRate: 10,
            repeat: -1,
            repeatDelay: 5000,
        };
        const key: string = 'cycle';
        super(manager, key, config);
    }
}