import {Types, Game as PhaserGame} from 'phaser';
import React, { Component } from 'react';
import SceneManager from "../Scenes/SceneManager";
import appConfig from "../app-config";
import UndergroundStage from "../Scenes/UndergroundStage";

const {debug} = appConfig;
export const config: Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 500 },
            debug
        }
    },
    pixelArt: true,
    roundPixels: true,
    scene: [SceneManager, UndergroundStage],
    parent: 'game',
    title: 'Orange Man',
};

interface GameProps {
    initializeGame(game: PhaserGame | null):  void;
    game: PhaserGame | null;
}

export default class Game extends Component<GameProps> {
    componentDidMount(){
        this.props.initializeGame(new PhaserGame(config));
    }
    render(){
        return <div id="game"/>;
    }
}