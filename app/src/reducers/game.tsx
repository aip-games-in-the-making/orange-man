import { GET_GAME, SET_GAME } from "../actions/game";
import {Game} from 'phaser';
import {AnyAction} from 'redux';

export type GameState = Game | null;

const defaultState: GameState = null;

/**
 * The game state
 * Usually, the store properties are simple objects. In this case, the game reducer's state is a Phaser.Game.
 * This game is either retrieved or set.
 * @param state
 * @param action
 */
export const game = (state: GameState = defaultState, action: AnyAction) => {
    switch(action.type){
        case GET_GAME:
            return state;
        case SET_GAME:
            return action.payload;
    }
};