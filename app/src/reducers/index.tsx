import {GameState} from "./game";

export interface AppState {
    game: GameState;
}