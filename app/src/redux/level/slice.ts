import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export interface LevelSlice{
    level: number;
    captive: Captive[];
    doors: Door[];
}

export interface Captive{
    critter: string;
    cage: string;
}

export interface Door{
    open: boolean;
    name: string;
}

export const slice = createSlice({
    name: 'level',
    initialState: {
        level: 0,
        captive: [],
        doors: []
    } as LevelSlice,
    reducers: {
        addCaptive: (state, action: PayloadAction<Captive>) => {
            const {critter, cage} = action.payload
            state.captive.push({
                critter,
                cage
            });
        },
        removeCaptive: (state, action: PayloadAction<Captive>) => {
            const {critter, cage} = action.payload
            state.captive = state.captive.filter((captive: Captive) => !(critter === captive.critter && cage === captive.cage));
        },
        clearCaptive: (state) => {
            state.captive = [];
        },
        addDoors: (state, action: PayloadAction<Door>) => {
            const {open, name} = action.payload
            state.doors.push({
                open,
                name
            });
        },
        openDoor: (state, action: PayloadAction<string>) => {
            state.doors = state.doors.map((door: Door) => {
                if(door.name === action.payload){
                    door.open = true;
                }
                return door;
            })
        },
        clearDoors: (state) => {
            state.doors = [];
        },
        setLevel: (state, action: PayloadAction<number>) => {
            state.level = action.payload;
        }
    }
})

export const {
    addCaptive,
    removeCaptive,
    clearCaptive,
    addDoors,
    openDoor,
    clearDoors,
    setLevel} = slice.actions;

export default slice.reducer;