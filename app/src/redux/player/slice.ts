import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Math as PhaserMath} from "phaser";

export const PLAYER_INIT_HEALTH = 3;
export const PLAYER_MAX_HEALTH = 10;
export const PLAYER_FATAL_HEALTH = -1;

export interface PlayerSlice{
    health: integer;
    canBeDamaged: boolean;
    currentMaxHealth: integer;
}

export const slice = createSlice({
    name: 'player',
    initialState: {
        health: PLAYER_INIT_HEALTH,
        canBeDamaged: true,
        currentMaxHealth: PLAYER_INIT_HEALTH
    } as PlayerSlice,
    reducers: {
        setHealth: (state, action: PayloadAction<integer>) => {
            state.health = PhaserMath.Clamp(action.payload, PLAYER_FATAL_HEALTH, state.currentMaxHealth);
        },
        adjustHealth: (state, action: PayloadAction<integer>) => {
            state.health = PhaserMath.Clamp(state.health + action.payload, PLAYER_FATAL_HEALTH, state.currentMaxHealth);
        },
        toggleDamage: (state, action:PayloadAction<boolean>) => {
            state.canBeDamaged = action.payload;
        }
    }
})

export const {
    setHealth,
    adjustHealth,
    toggleDamage
} = slice.actions;

export default slice.reducer;