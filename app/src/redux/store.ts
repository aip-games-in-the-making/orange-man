import { configureStore } from '@reduxjs/toolkit';
import level from './level/slice';
import player from './player/slice';

const store = configureStore({
    reducer: {
        level,
        player,
    },
});
export default store;
export type RootState = ReturnType<typeof store.getState>